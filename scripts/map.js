var map;

function initMap() {
var center = {lat: 52.9906028, lng: 18.6674536}; //52.9908353,18.6669601
var location = {lat: 52.990744, lng: 18.666897};
map = new google.maps.Map(document.getElementById('map'), {
    center: center,
    zoom: 17
});

var infowindow = new google.maps.InfoWindow;
        infowindow.setContent('<b>Linnet automation</b></br>ul. Włocławska 167</br>87-100 Toruń</br>Polska');
    
var marker = new google.maps.Marker({map: map, position: location});

marker.addListener('click', function() {
    infowindow.open(map, marker);
});

}