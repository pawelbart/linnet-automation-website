function scrollMenu(menuBtn, sectionId) {
    $(menuBtn).click(function() {
        $('html, body').animate({
            scrollTop: $(sectionId).offset().top
        }, 500);
    });
}

scrollMenu("#btn-about-us", "#about-us");
scrollMenu("#btn-scada", "#scada");
scrollMenu("#btn-explore", "#scada");
scrollMenu("#btn-demo", "#demo");
scrollMenu("#btn-contact", "#contact");
scrollMenu("#btn-home", "#header");

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("btn-home").style.opacity = "1";
        document.getElementById("btn-home").style.cursor = "pointer";
    } else {
        document.getElementById("btn-home").style.opacity = "0";
        document.getElementById("btn-home").style.cursor = "default"; 
    }
}